/* eslint-disable no-undef */
import { createRouter, createWebHistory } from 'vue-router';
const routes = [
  {
    path: '/',
        name: 'index',
        component: () => import('../Test.vue')
    },
    {
        path: '/search',
        name: 'search',
        component: () => import('../views/Search.vue')
    }
];

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes
});

  
export default router;

using Dapper.Contrib.Extensions;

namespace webapi.Models;

[Table("weatherForecast")]
public class WeatherForecast
{
    public int id { get; set; }
    public DateTime date { get; set; }

    public int temperatureC { get; set; }

    public int temperatureF => 32 + (int)(temperatureC / 0.5556);

    public string? summary { get; set; }
}

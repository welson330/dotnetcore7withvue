﻿using Dapper;
using System.Data;
using webapi.Models;

namespace webapi.Repositories
{
    public class WeatherForecastRepository : IWeatherForecastRepository
    {
        private readonly IDatabaseHelper _databaseHelper;
        public WeatherForecastRepository(IDatabaseHelper databaseHelper)
        {
            this._databaseHelper = databaseHelper;
        }
        public void AddWeatherForecast(WeatherForecast weatherForecast)
        {
            using (IDbConnection conn = this._databaseHelper.GetConnection())
            {
                string sql = @"INSERT INTO WeatherForecast (date,temperatureC,summary)  VALUES (@date, @temperatureC, @summary);";
                conn.Execute(sql, weatherForecast);
            }
        }

        public List<WeatherForecast> GetWeatherForecast()
        {
            using (IDbConnection conn = this._databaseHelper.GetConnection())
            {
                string sql = @"SELECT * FROM WeatherForecast";

                return conn.Query<WeatherForecast>(sql).ToList();
            }
        }

        public void RemoveWeatherForecast(int id)
        {
            using (IDbConnection conn = this._databaseHelper.GetConnection())
            {
                string sql = "DELETE FROM WeatherForecast where id = @id";
                conn.Execute(sql, new { id = id });
            }
        }

        public void UpdateWeatherForecast(WeatherForecast weatherForecast)
        {
            using (IDbConnection conn = this._databaseHelper.GetConnection())
            {
                string sql = @"UPDATE WeatherForecast SET date = @date, temperatureC = @temperatureC, summary = @summary WHERE id = @id";
                conn.Execute(sql, weatherForecast);
            }
        }
    }
}

﻿
using webapi.Models;

namespace webapi.Repositories
{
    public interface IWeatherForecastRepository
    {

        public void AddWeatherForecast(WeatherForecast weatherForecast);

        public List<WeatherForecast> GetWeatherForecast();

        public void RemoveWeatherForecast(int id);

        public void UpdateWeatherForecast(WeatherForecast weatherForecast);
    }

}

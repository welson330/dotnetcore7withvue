﻿using Microsoft.AspNetCore.Mvc;
using webapi.Models;
using webapi.Repositories;

namespace webapi.Controllers;

[ApiController]
[Route("api/[controller]")]
public class WeatherForecastController : ControllerBase
{
    private IWeatherForecastRepository _weatherForecastRepository;

    public WeatherForecastController(IWeatherForecastRepository weatherForecastRepository)
    {
        this._weatherForecastRepository = weatherForecastRepository;
    }

    [HttpGet("[action]")]
    public IActionResult GetWeatherForecast()
    {
        return Ok(getWeatherForecastList());
    }

    [HttpPost("[action]")]
    public IActionResult SaveWeatherForecast([FromForm] WeatherForecast weatherForecast)
    {
        this._weatherForecastRepository.AddWeatherForecast(weatherForecast);
        return Ok(getWeatherForecastList());
    }

    [HttpPost("[action]")]
    public IActionResult UpdateWeatherForecast([FromForm] WeatherForecast weatherForecast)
    {
        this._weatherForecastRepository.UpdateWeatherForecast(weatherForecast);

        return Ok(getWeatherForecastList());
    }

    //[HttpGet("[action]/{id}")]
    //public IActionResult GetWeatherForecast(int id)
    //{
    //    return Ok(_weatherForecastList.FirstOrDefault(item => item.id == id));
    //}
    [HttpGet("[action]/{id}")]
    public IActionResult DeleteWeatherForecast(int id)
    {
        this._weatherForecastRepository.RemoveWeatherForecast(id);
        return Ok(getWeatherForecastList());
    }

    private List<WeatherForecast> getWeatherForecastList()
    {
        return this._weatherForecastRepository.GetWeatherForecast();
    }
}
